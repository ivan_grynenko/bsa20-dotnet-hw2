﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UI
{
    class Program
    {
        private static string[] menu =
            {
                "Replenish the account of a vehicle",
                "Add a vehicle to the parking lot",
                "Remove a vehicle from the parking lot",
                "The number of available places",
                "Current period income",
                "Overall income",
                "Display all vehicles on the parking lot",
                "Display all current transactions",
                "Display the history of transactions",
                "Exit"
            };

        static async Task Main(string[] args)
        {
            HttpClient http = new HttpClient();
            http.BaseAddress = new Uri("https://localhost:44386/");

            while (true)
            {
                Console.Clear();
                int menuPoint = 0;
                ConsoleKeyInfo userInput;

                do
                {
                    DisplayMenu(menuPoint);
                    userInput = Console.ReadKey();

                    if (userInput.Key == ConsoleKey.UpArrow)
                        menuPoint = DisplayMenu(--menuPoint);

                    if (userInput.Key == ConsoleKey.DownArrow)
                        menuPoint = DisplayMenu(++menuPoint);

                } while (userInput.Key != ConsoleKey.Enter);

                if (menu[menuPoint] == "Exit")
                {
                    http.Dispose();
                    Environment.Exit(0);
                }

                if (menu[menuPoint] == "Add a vehicle to the parking lot")
                {
                    SetInternalDisplay();

                    Console.WriteLine("Enter a plate number: ");
                    var vehicleId = Console.ReadLine();

                    Console.WriteLine("Initial balance: ");
                    var vehicleBalance = GetNumInput();

                    Console.WriteLine("State a type of the car. It has to be one of the following: ");
    
                    var vehicleType = (VehicleType)GetType();

                    try
                    {
                        var newVehicle = new Vehicle(vehicleId, vehicleType, (decimal)vehicleBalance);
                        var response = await http.PostAsJsonAsync("api/vehicles", newVehicle);

                        if (response.StatusCode == HttpStatusCode.Created)
                        {
                            Console.WriteLine("Successfully added");
                        }
                        else
                        {
                            Console.WriteLine(await response.Content.ReadAsStringAsync());
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Replenish the account of a vehicle")
                {
                    Console.WriteLine("Enter a plate number: ");
                    var vehicleId = Console.ReadLine();

                    Console.WriteLine("Depositing amount: ");
                    var vehicleBalance = GetNumInput();

                    try
                    {
                        var response = await http.PutAsJsonAsync("api/transactions/topUpVehicle", new { Id = vehicleId, Balance = vehicleBalance });
                        
                        if (response.IsSuccessStatusCode)
                            Console.WriteLine("The sum was deposited");
                        else
                        {
                            throw new Exception(await response.Content.ReadAsStringAsync());
                        }
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "Remove a vehicle from the parking lot")
                {
                    Console.WriteLine("Enter a plate number: ");
                    var vehicleId = Console.ReadLine();

                    try
                    {
                        var response = await http.DeleteAsync($"api/vehicles/{vehicleId}");

                        if (response.StatusCode == HttpStatusCode.NoContent)
                            Console.WriteLine("The vehicle was removed");
                    }
                    catch (Exception e)
                    {
                        ErrorHandler(e);
                    }
                }

                if (menu[menuPoint] == "The number of available places")
                {
                    int availableLots = 0;
                    var response = await http.GetAsync("api/parking/freePlaces");

                    if (response.IsSuccessStatusCode)
                        availableLots = await response.Content.ReadAsAsync<int>();

                    Console.WriteLine(availableLots);
                }

                if (menu[menuPoint] == "Current period income")
                {
                    var response = await http.GetAsync("api/transactions/last");

                    if (response.IsSuccessStatusCode)
                    {
                        var currentTransactions = await response.Content.ReadAsAsync<TransactionInfo[]>();
                        decimal sum = 0;

                        foreach (var transaction in currentTransactions)
                            sum += transaction.Sum;

                        Console.WriteLine(sum);
                    }
                }

                if (menu[menuPoint] == "Overall income")
                {
                    var response = await http.GetAsync("api/parking/balance");

                    if (response.IsSuccessStatusCode)
                    {
                        var overallBalance = await response.Content.ReadAsAsync<decimal>();
                        Console.WriteLine(overallBalance);
                    }
                }

                if (menu[menuPoint] == "Display all vehicles on the parking lot")
                {
                    var response = await http.GetAsync("api/vehicles");

                    if (response.IsSuccessStatusCode)
                    {
                        var vehicles = await response.Content.ReadAsAsync<IEnumerable<Vehicle>>();

                        if (vehicles.Count() > 0)
                        {
                            foreach (var vehicle in vehicles)
                                Console.WriteLine($"{vehicle.Id}");
                        }
                        else
                        {
                            Console.WriteLine("No vehicles on the parking lot");
                        }
                    }
                }

                if (menu[menuPoint] == "Display all current transactions")
                {
                    var response = await http.GetAsync("api/transactions/last");

                    if (response.IsSuccessStatusCode)
                    {
                        var currentTransactions = await response.Content.ReadAsAsync<TransactionInfo[]>();

                        if (currentTransactions.Length > 0)
                        {
                            foreach (var transaction in currentTransactions)
                                Console.WriteLine($"Transaction details: ${transaction.Sum} from {transaction.VehicleId} at {transaction.Date.ToString("dddd, dd MMMM yyyy HH:mm:ss")}");

                        }
                        else
                        {
                            Console.WriteLine("No transactions were made yet");
                        }
                    }
                }

                if (menu[menuPoint] == "Display the history of transactions")
                {
                    var allTransactions = await Task.Run(async () =>
                    {
                        string text = "";

                        try
                        {
                            text = await http.GetStringAsync("api/transactions/all");
                        }
                        catch (Exception e)
                        {
                            ErrorHandler(e);
                        }

                        return text;
                    });
                    Console.WriteLine(allTransactions.Length > 0 ? allTransactions : "No transactions were saved yet");
                }

                Console.WriteLine("Press any key ro return");
                Console.ReadKey();
            }
        }

        private static int DisplayMenu(int selectedRow)
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine("Use UpArrow and DownArrow keys to navigate. Then press Enter. Select \"Exit\" to close the app." + Environment.NewLine);

            if (selectedRow < 0)
                selectedRow = 0;

            if (selectedRow > menu.Length - 1)
                selectedRow = menu.Length - 1;

            for (int i = 0; i < menu.Length; i++)
            {
                if (i == selectedRow)
                {
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine(menu[i]);
                    Console.ResetColor();
                }
                else
                    Console.WriteLine(menu[i]);
            }

            return selectedRow;
        }

        private static void SetInternalDisplay()
        {
            Console.Clear();
            Console.SetCursorPosition(0, 0);
        }

        private static double GetNumInput()
        {
            var input = Console.ReadLine();
            double result;

            while (!double.TryParse(input, out result))
            {
                Console.WriteLine("It has to be a number. Try again!");
                input = Console.ReadLine();
            }

            return result;
        }

        private static void ErrorHandler(Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(e.Message);
            Console.ResetColor();
        }

        private static int GetType()
        {
            var types = Enum.GetNames(typeof(VehicleType));

            var r = new Regex(@"(?<=[A-Z])(?=[A-Z][a-z]) | (?<=[^A-Z])(?=[A-Z]) | (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            var typesFinal = types.Select(e => new { Type = r.Replace(e, " ") });

            int i = 1;
            foreach (var item in typesFinal)
                Console.WriteLine($"{i++}. {item.Type}");

            double userInput;
            do
            {
                userInput = GetNumInput();

                if (userInput < 1 || userInput > typesFinal.Count())
                    Console.WriteLine("Select one of those above");

            } while (userInput < 1 || userInput > typesFinal.Count());

            return (int)userInput - 1;
        }
    }
}
