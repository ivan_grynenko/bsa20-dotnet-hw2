﻿using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        private object locker = new object();

        public LogService(string path)
        {
            LogPath = path;
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("Transactions file doesn't exist");

            using (var reader = new StreamReader(LogPath, Encoding.UTF8))
            {
                lock (locker)
                    return reader.ReadToEnd();
            }
        }

        public void Write(string logInfo)
        {
            using (var writer = new StreamWriter(LogPath, true, Encoding.UTF8))
            {
                lock (locker)
                    writer.WriteAsync(logInfo + Environment.NewLine);
            }
        }
    }
}
