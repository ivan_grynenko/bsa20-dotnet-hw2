﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking;
        private readonly ILogService logService;
        private readonly ITimerService withdrawTimer;
        private readonly ITimerService logTimer;
        private List<TransactionInfo> transactions;
        private bool isDisposed = false;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking = Parking.Instance;
            parking.Vehicles = new List<Vehicle>();
            parking.Balance = 0;

            transactions = new List<TransactionInfo>();

            this.logService = logService;
            this.withdrawTimer = withdrawTimer;
            withdrawTimer.Interval = Settings.ChargingPeriod * 1000;
            this.logTimer = logTimer;
            logTimer.Interval = Settings.LogingPeriod * 1000;

            SetWithdrawTimer();
            SetLogTimer();
            logTimer.Start();
            withdrawTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0)
                throw new InvalidOperationException("No vacant parking lots");

            var exstingVehicle = parking.Vehicles.FirstOrDefault(e => e.Id == vehicle.Id);

            if (exstingVehicle != null)
                throw new ArgumentException("Such Id already exists");

            parking.Vehicles.Add(vehicle);
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = parking.Vehicles.FirstOrDefault(e => e.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Such Id doesn't exist");

            if (vehicle.Balance < 0)
                throw new InvalidOperationException("The vehicle has a negative balance");

            parking.Vehicles.Remove(vehicle);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum < 0)
                throw new ArgumentException("The amoun can't be negative");

            var vehicle = parking.Vehicles.FirstOrDefault(e => e.Id == vehicleId);

            if (vehicle == null)
                throw new ArgumentException("Such Id doesn't exist");

            vehicle.Balance += sum;
        }

        private void SetLogTimer()
        {
            logTimer.Elapsed += async (s, e) =>
            {
                var log = new StringBuilder();
                await Task.Run(() => transactions.ForEach(e => 
                log.AppendFormat($"Transaction details: ${e.Sum} from {e.VehicleId} at {e.Date.ToString("dddd, dd MMMM yyyy HH:mm:ss")}\n")));

                logService.Write(log.ToString());

                transactions = new List<TransactionInfo>();
            };
        }

        private void SetWithdrawTimer()
        {
            withdrawTimer.Elapsed += (s, e) =>
            {
                parking.Vehicles.ForEach(e =>
                {
                    var fee = GetAmount(e);
                    e.Balance -= fee;
                    parking.Balance += fee;
                    var transaction = new TransactionInfo { Sum = fee, VehicleId = e.Id, Date = DateTime.Now };
                    transactions.Add(transaction);
                });
            };
        }

        private decimal GetAmount(Vehicle vehicle)
        {
            decimal amount = 0;
            var balance = vehicle.Balance;

            switch (vehicle.VehicleType)
            {
                case VehicleType.PassengerCar:
                    amount += Settings.PassengerCar;
                    break;
                case VehicleType.Truck:
                    amount += Settings.Truck;
                    break;
                case VehicleType.Bus:
                    amount += Settings.Bus;
                    break;
                case VehicleType.Motorcycle:
                    amount += Settings.Motorcycle;
                    break;
                default:
                    break;
            }

            if (balance - amount > 0)
                return amount;
            else
                return balance > 0 ? balance + (amount - balance) * Settings.Penalty : amount * Settings.Penalty;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposed)
            {
                if (isDisposing)
                {
                    parking.Dispose();
                    logTimer.Dispose();
                    withdrawTimer.Dispose();
                }

                isDisposed = true;
            }
        }

        ~ParkingService()
        {
            Dispose(false);
        }
    }
}
