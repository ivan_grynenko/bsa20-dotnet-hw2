﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;

        public double Interval { get => timer.Interval; set => timer.Interval = value; }

        public event ElapsedEventHandler Elapsed { add => timer.Elapsed += value; remove => timer.Elapsed -= value; }

        public TimerService()
        {
            timer = new Timer(1000);
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer = null;
        }
    }
}
