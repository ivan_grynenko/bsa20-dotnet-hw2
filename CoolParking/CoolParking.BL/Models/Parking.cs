﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Parking instanceOfParking;
        public decimal Balance { get; set; }
        public List<Vehicle> Vehicles { get; set; }
        private static object locker = new object();

        public static Parking Instance
        {
            get
            {
                if (instanceOfParking == null)
                {
                    lock (locker)
                        instanceOfParking = new Parking();
                }
                return instanceOfParking;
            }
        }

        public void Dispose()
        {
            Vehicles = null;
        }
    }
}
