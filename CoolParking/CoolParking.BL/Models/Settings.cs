﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance { get; }
        public static int Capacity { get; }
        public static int ChargingPeriod { get; }
        public static int LogingPeriod { get; }
        public static decimal PassengerCar { get; }
        public static decimal Truck { get; }
        public static decimal Bus { get; }
        public static decimal Motorcycle { get; }
        public static decimal Penalty { get; set; }

        static Settings()
        {
            Balance = 0;
            Capacity = 10;
            ChargingPeriod = 5;
            LogingPeriod = 60;
            PassengerCar = 2;
            Truck = 5m;
            Bus = 3.5m;
            Motorcycle = 1m;
            Penalty = 2.5m;
        }
    }
}
