﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime Date { get; set; }
    }
}
