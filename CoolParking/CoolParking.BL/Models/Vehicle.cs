﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }
        private string idPattern = @"^[A-Z]{2}-\d{4}-[A-Z]{2}$";

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (!Regex.IsMatch(id, idPattern))
                throw new ArgumentException("Wrong Id");

            if (balance < 0)
                throw new ArgumentException("Balance cannot be negative");

            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var random = new Random();
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var partOfId = new string[4]
                .Select(e => chars[random.Next(0, chars.Length)]);
            var plateNumber = new StringBuilder(new string(partOfId.Take(2).ToArray()))
                .Append("-");

            for (int i = 0; i < 4; i++)
                plateNumber.Append(random.Next(9).ToString());

            plateNumber.Append("-")
                .Append(new string(partOfId.Skip(2).Take(2).ToArray()));
            return plateNumber.ToString();
        }
    }
}
