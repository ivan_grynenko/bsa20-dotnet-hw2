﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet("last")]
        public async Task<ActionResult<IEnumerable<TransactionInfo>>> GetLastTransactions()
        {
            var transactions = await Task.Run(() => parkingService.GetLastParkingTransactions());
            return Ok(transactions);
        }

        [HttpGet("all")]
        public async Task<ActionResult<string>> GetAllTransactions()
        {
            string log = "";
            try
            {
                log = parkingService.ReadFromLog();
            }
            catch (InvalidOperationException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return Ok(log);
        }

        [HttpPut("topUpVehicle")]
        public async Task<ActionResult<Vehicle>> TopUpVehicle([FromBody]VehicleTopUpModel data)
        {
            if (!ModelState.IsValid || data.Sum < 0)
                return BadRequest(new { message = "Inappropriate data" });

            if (!Regex.IsMatch(data.Id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
                return BadRequest(new { message = "Not valid id" });

            try
            {
               parkingService.TopUpVehicle(data.Id, data.Sum);
            }
            catch (Exception)
            {
                return NotFound(new { messsage = "Such id doesn't exist" });
            }

            var vehicle = await Task.Run(() => parkingService.GetVehicles()
                .FirstOrDefault(e => e.Id == data.Id));

            return Ok(vehicle);
        }
    }
}