﻿using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet("balance")]
        public async Task<ActionResult<decimal>> GetBalance()
        {
            var balance = await Task.Run(() => parkingService.GetBalance());
            return Ok(balance);
        }

        [HttpGet("capacity")]
        public async Task<ActionResult<int>> GetCapacity()
        {
            var capacity = await Task.Run(() => parkingService.GetCapacity());
            return Ok(capacity);
        }

        [HttpGet("freePlaces")]
        public async Task<ActionResult<int>> GetFreePlaces()
        {
            var freePlaces = await Task.Run(() => parkingService.GetFreePlaces());
            return Ok(freePlaces);
        }
    }
}