﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using CoolParking.WebAPI.Models;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Vehicle>>> GetVehicles()
        {
            var vehicles = await Task.Run(() => parkingService.GetVehicles());
            return Ok(vehicles);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Vehicle>> GetVehiclesById(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
                return BadRequest(new { message = "Not valid id" });

            var vehicle = await Task.Run(() => parkingService.GetVehicles()
                .FirstOrDefault(e => e.Id == id));


            if (vehicle == null)
                return NotFound(new { message = "No vehicles with such id" });

            return Ok(vehicle);
        }

        [HttpPost]
        public ActionResult<VehicleModel> AddVehicle([FromBody]VehicleModel newVehicle)
        {
            if (!ModelState.IsValid || string.IsNullOrEmpty(newVehicle.Id))
                return BadRequest(new { message = "Some data is missing" });

            try
            {
                var vehicle = new Vehicle(newVehicle.Id, newVehicle.VehicleType, newVehicle.Balance);
                parkingService.AddVehicle(vehicle);
            }
            catch (Exception e)
            {
                return BadRequest(new { message = "Something went wrong" });
            }

            return CreatedAtAction(nameof(GetVehiclesById), new { id = newVehicle.Id }, newVehicle);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicle(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-\d{4}-[A-Z]{2}$"))
                return BadRequest(new { message = "Not valid id" });

            try
            {
                parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

            return NoContent();
        }
    }
}