﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class VehicleModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        [EnumDataType(typeof(VehicleType))]
        public VehicleType VehicleType { get; set; }
        [Required]
        public decimal Balance { get; set; }
    }
}
