﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class VehicleTopUpModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public decimal Sum { get; set; }
    }
}
